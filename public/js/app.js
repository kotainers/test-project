/**
 * Created by kotainer on 18.09.2016.
 */
//var ws = new WebSocket("ws://localhost:8088");
var ws = new WebSocket("ws://192.168.0.108:8088");
(function() {
    // Проверим, поддерживает ли браузер уведомления
    if (!("Notification" in window)) {
        alert("Ваш браузер не поддерживает HTML5 Notifications");
    }
    // В противном случае, мы должны спросить у пользователя разрешение
    else if (Notification.permission === 'default') {
        Notification.requestPermission(function (permission) {
            // Не зависимо от ответа, сохраняем его в настройках
            if(!('permission' in Notification)) {
                Notification.permission = permission;
            }
        });
    }

    var app = angular.module('textsList', []);

    app.controller('List',function($scope){
        $scope.myBlockId = 0;
        $scope.name = '';
        $scope.log = false;

        ws.onmessage = function(event) {
            var msg = JSON.parse(event.data);
            if (msg.act == 'alert') {
                showNotification(msg.data);
            }
            if (msg.act == 'fullList') {
                if (!$scope.$$phase) $scope.$apply(function () {
                    $scope.fullList = msg.data;
                });
            }
            if (msg.act == 'log') {
                if (msg.data == 'OK'){
                    $scope.log = true;
                    ws.send(JSON.stringify({
                        act: 'getAll'
                    }));
                }
            }
            if (msg.act == 'newElem') {
                if (!$scope.$$phase) $scope.$apply(function () {
                    $scope.fullList.push(msg.data);
                });
            }
            if (msg.act == 'delElem') {
                if (!$scope.$$phase) $scope.$apply(function () {
                    for(var i=0; i < $scope.fullList.length; i++) {
                        if(msg.id == $scope.fullList[i]._id) {
                            $scope.fullList.splice(i,1);
                            break;
                        }
                    }
                });
            }
            if (msg.act == 'blockElem') {
                if($scope.myBlockId != msg.id) {
                    if (!$scope.$$phase) $scope.$apply(function () {
                        for (var i = 0; i < $scope.fullList.length; i++) {
                            if (msg.id == $scope.fullList[i]._id) {
                                $scope.fullList[i].blocking = msg.block;
                                $scope.fullList[i].user_block = msg.name;
                                break;
                            }
                        }
                    });
                }
            }
            if (msg.act == 'changeElem') {
                if($scope.myBlockId != msg.id) {
                    if (!$scope.$$phase) $scope.$apply(function () {
                        for (var i = 0; i < $scope.fullList.length; i++) {
                            if (msg.id == $scope.fullList[i]._id) {
                                $scope.fullList[i].text = msg.text;
                                break;
                            }
                        }
                    });
                }
            }
        };
        $scope.addNew = function () {
            ws.send(JSON.stringify({
                act: 'addNew'
            }));
        };
        $scope.delElem = function (id) {
            ws.send(JSON.stringify({
                act: 'delElem',
                id: id
            }));
        };
        $scope.blockElem = function (id, block) {
            if(block) {
                $scope.myBlockId = id;
                for (var i = 0; i < $scope.fullList.length; i++) {
                    if ($scope.myBlockId== $scope.fullList[i]._id) {
                        $scope.fullList[i].blocking = 'edit';
                        console.log('edit');
                        break;
                    }
                }
            }
            else
                $scope.myBlockId = 0;
            ws.send(JSON.stringify({
                act: 'blockElem',
                id: id,
                block: block,
                name: $scope.name
            }));
        };
        $scope.changeElem = function (id, val) {
            ws.send(JSON.stringify({
                act: 'changeElem',
                id: id,
                text: val.currentTarget.value,
                name: $scope.name
            }));
        }
        $scope.log = function () {
            if($scope.name.length > 2)
                ws.send(JSON.stringify({
                    act: 'log',
                    name: $scope.name
                }));
            else showNotification('Введите Ваше имя')
        }

    });
})();

function showNotification(msg) {
    if (Notification.permission === 'granted'){
        var notification = new Notification('Тестовый проект', {
            lang: 'ru-RU',
            body: msg,
            icon: '/images/icon.png'
        });
        setTimeout(notification.close.bind(notification), 3000);
    }
    else{
        alert(msg);
    }
}