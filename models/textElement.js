/**
 * Created by kotainer on 14.10.2016.
 */
var mongoose = require('../lib/mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    text: { //Логин входа на сайт
        type: String
    },
    blocking: {
        type: Boolean
    },
    user_block: {
        type: String
    }
});
// создание нового элемента
schema.statics.create = function(callback) {
    var Elem = this;
    var elem = new Elem({text: "", blocking: false,  user_block:""});
    elem.save(function(err) {
        if (err) return callback(false);
        callback(elem);
    });
};
// получение всех
schema.statics.getList = function(callback){
    var Elem = this;
    Elem.find({}).exec(callback);
};
// изменение текста
schema.statics.changeText = function(id, newText, blocking, username, callback) {
    var Elem  = this;
    Elem.update({_id: id},
        {
            text: newText,
            blocking: blocking,
            user_block: username
        }, callback);
};
// изменить блокировку
schema.statics.changeBlock = function(id, blocking, username, callback) {
    var Elem  = this;
    Elem.update({_id: id},
        {
            blocking: blocking,
            user_block: username
        }, callback);
};
// снять блокировку по имени
schema.statics.unlock = function(username, callback) {
    var Elem  = this;
    Elem.update({user_block: username},
        {
            blocking: false
        }, callback);
};
// удалить
schema.statics.delete = function(id, callback) {
    var Elem  = this;
    Elem.remove({_id: id}, callback);
};

exports.Element = mongoose.model('Element', schema);