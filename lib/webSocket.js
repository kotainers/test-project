/**
 * Created by kotainer on 14.10.2016.
 */
var Element = require('../models/textElement').Element;
var WebSocketServer = new require('ws');

var webSocketServer = new WebSocketServer.Server({port: 8088});

var users = [];
var usedNames = [];
// отправка всем пользователям, которые онлайн
function sendAllUsers (data) {
    users.forEach(function(user) {
        user.send(data);
    });
}

webSocketServer.on('connection', function(ws) {
    users.push(ws);
    ws.on('message', function(message) {
        try{
            var msg = JSON.parse(message);
            // Авторизация
            if (msg.act =='log') {
                if(usedNames.indexOf(msg.name) == -1){
                    usedNames.push(msg.name);
                    for(var i=0; i < users.length; i++) {
                        if(ws == users[i]) {
                            users[i].name = msg.name;
                            break;
                        }
                    }
                    ws.send(JSON.stringify(
                        {
                            act: 'log',
                            data: 'OK'
                        }));
                }
                else {
                    ws.send(JSON.stringify(
                        {
                            act: 'alert',
                            data: 'Данные имя уже занято, пожалуйста введите другое'
                        }));
                }
            }
            // Новый элемент
            if (msg.act =='addNew') {
                Element.create(function (res) {
                    sendAllUsers(JSON.stringify(
                        {
                            act: "newElem",
                            data: res
                        }));
                });
            }
            // Получить весь список элементов
            if (msg.act =='getAll') {
                Element.getList(function (err, elem) {
                    if(err) {
                        ws.send(JSON.stringify(
                            {
                                act: 'alert',
                                data: 'Возникла ошибка, но мы всё исправим!'
                            }));
                    }
                    else {
                        ws.send(JSON.stringify(
                            {
                                act: "fullList",
                                data: elem
                            }));
                    }
                });
            }
            // Удалить элемент
            if (msg.act =='delElem') {
                Element.delete(msg.id, function () {
                    sendAllUsers(JSON.stringify(
                        {
                            act: "delElem",
                            id: msg.id
                        }));
                });
            }
            // Заблокировать элемент
            if (msg.act =='blockElem') {
                Element.changeBlock(msg.id, msg.block, msg.name, function () {
                    sendAllUsers(JSON.stringify(
                        {
                            act: "blockElem",
                            id: msg.id,
                            block: msg.block,
                            name: msg.name
                        }));
                });
            }
            // Изменение значения
            if (msg.act =='changeElem') {
                Element.changeText(msg.id, msg.text, true, msg.name, function () {
                    sendAllUsers(JSON.stringify(
                        {
                            act: "changeElem",
                            id: msg.id,
                            text: msg.text,
                            name: msg.name
                        }));
                });
            }
        }
        catch(e) {
            console.error(e);
        }
    });

    ws.on('close', function() {
        for(var i=0; i < users.length; i++) {
            if(ws == users[i]) {
                users.splice(i,1);
                // Разблокируем все элементы, связанные с данным пользователем
                Element.unlock(usedNames[i], function (err) {
                    if(err) console.log(err);
                    Element.getList(function (err, elem) {
                        sendAllUsers(JSON.stringify(
                            {
                                act: "fullList",
                                data: elem
                            }));
                    });
                });
                usedNames.splice(i,1);
                break;
            }
        }
    });
});