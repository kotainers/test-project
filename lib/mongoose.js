/**
 * Created by kotainer on 14.10.2016.
 */
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var opts = {
    "server": {
        "socketOptions": {
            "keepAlive": 1
        }
    }
};
mongoose.connect('mongodb://localhost/textsList', opts);

module.exports = mongoose;
